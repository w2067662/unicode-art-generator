# -*- coding: utf-8 -*-

#--------------------------------
#             Const
#--------------------------------
# Default Output Directory
DEFAULT_OUTPUT_DIRECTORY = "output"

# File Route Names
IMG_TO_UNICODE = "img2unicode"
IMG_TO_UNICODE_PLUS = "img2unicode_plus"
GIF_TO_COLORED_UNICODE_GIF = "gif2colored_unicode_gif"
TEXT_TO_LARGE_TEXT = "text2large_text"
