#!/bin/bash
set -e

# Set the path to your Python executable
PYTHON_EXE=python3
# Set the name of your virtual environment
VENV_NAME=myenv

# Check if virtualenv is installed
if [ ! -d "$VENV_NAME" ]; then
    echo "Installing virtualenv..."
    $PYTHON_EXE -m pip install virtualenv
    if [ $? -ne 0 ]; then
        echo "Error installing virtualenv. Press any key to exit..."
        read -n 1 -s
        exit 1
    fi
fi

# Create and activate the virtual environment
if [ ! -d "$VENV_NAME" ]; then
    echo "Creating virtual environment..."
    $PYTHON_EXE -m virtualenv $VENV_NAME
    if [ $? -ne 0 ]; then
        echo "Error creating virtual environment. Press any key to exit..."
        read -n 1 -s
        exit 1
    fi
fi

echo "Activating virtual environment..."
source $VENV_NAME/bin/activate
if [ $? -ne 0 ]; then
    echo "Error activating virtual environment. Press any key to exit..."
    read -n 1 -s
    exit 1
fi

# Install dependencies
echo "Installing dependencies..."
pip install -r requirements.txt
if [ $? -ne 0 ]; then
    echo "Error installing dependencies. Press any key to exit..."
    read -n 1 -s
    exit 1
fi

# Run your Python program
echo "Running your Python program..."
python main.py
if [ $? -ne 0 ]; then
    echo "Error running your Python program. Press any key to exit..."
    read -n 1 -s
    exit 1
fi

# Deactivate the virtual environment
echo "Deactivating virtual environment..."
deactivate
if [ $? -ne 0 ]; then
    echo "Error deactivating virtual environment. Press any key to exit..."
    read -n 1 -s
    exit 1
fi

echo "Environment setup and program execution complete."
read -n 1 -s
exit 0
