# -*- coding: utf-8 -*-

#--------------------------------
#         Figlet Styles
#--------------------------------
# Define Figlet Styles
ALLIGATOR = "alligator"
ALLIGATOR2 = "alligator2"
BIG = "big"
BULBHEAD = "bulbhead"
CHUNKY = "chunky"
COINSTAK = "coinstak"
CONTESSA = "contessa"
CRICKET = "cricket"
DIGITAL = "digital"
DOH = "doh"
EFTIFONT = "eftifont"
DOOM = "doom"
DOTMATRIX = "dotmatrix"
ISOMETRIC1 = "isometric1"
ISOMETRIC2 = "isometric2"
ISOMETRIC3 = "isometric3"
ISOMETRIC4 = "isometric4"
LARRY3D = "larry3d"
MARQUEE = "marquee"
NTGREEK = "ntgreek"
OGRE = "ogre"
SLANT = "slant"
SMKEYBOARD = "smkeyboard"
SMISOME1 = "smisome1"
STOP = "stop"

# Define Figlet Styles Map
FIGLET_STYLES = [
    ALLIGATOR, ALLIGATOR2, BIG, BULBHEAD, CHUNKY, COINSTAK, CONTESSA, CRICKET,
    DIGITAL, DOH, EFTIFONT, DOOM, DOTMATRIX, ISOMETRIC1, ISOMETRIC2,
    ISOMETRIC3, ISOMETRIC4, LARRY3D, MARQUEE, NTGREEK, OGRE, SLANT, SMKEYBOARD,
    SMISOME1, STOP
]
