# -*- coding: utf-8 -*-

from figlet_styles import LARRY3D
from const import DEFAULT_OUTPUT_DIRECTORY
import sys, pyfiglet

#--------------------------------
#        Global Variables
#--------------------------------
__application__ = "UNICODE ART GENERATOR"
__author__ = ""
__version__ = '1.0.0'


#--------------------------------
#           Args Class
#--------------------------------
class Args:

    def __init__(self):
        self.help = False
        self.version = False
        self.webui = False
        self.share = False
        self.output = DEFAULT_OUTPUT_DIRECTORY
        self.scale = 1.0
        self.contrast_alpha = 1.0
        self.contrast_beta = 0
        self.sigma = 1.0
        self.invert = False
        self.plus = False
        self.image = None


#--------------------------------
#           get_args()
#--------------------------------
def get_args():
    args = Args()

    i = 0
    while i < len(sys.argv):
        option = sys.argv[i]

        if option in ['-h', '--help']:
            args.help = True
        elif option in ['-v', '--version']:
            args.version = True
        elif option in ['-w', '--webui']:
            args.webui = True
        elif option in ['-s', '--share']:
            args.share = True
        elif option in ['-O', '--output']:
            try:
                args.output = sys.argv[i + 1]
                i += 1
            except (IndexError, ValueError):
                print("Error: Output file not provided.")
                sys.exit(1)
        elif option in ['-S', '--scale']:
            try:
                args.scale = float(sys.argv[i + 1])
                i += 1
            except (IndexError, ValueError):
                print("Error: Invalid scale value.")
                sys.exit(1)
        elif option in ['-CA', '--contrast_alpha']:
            try:
                args.contrast_alpha = float(sys.argv[i + 1])
                i += 1
            except (IndexError, ValueError):
                print("Error: Invalid contrast_alpha value.")
                sys.exit(1)
        elif option in ['-CB', '--contrast_beta']:
            try:
                args.contrast_beta = int(sys.argv[i + 1])
                i += 1
            except (IndexError, ValueError):
                print("Error: Invalid contrast_beta value.")
                sys.exit(1)
        elif option in ['-SG', '--sigma']:
            try:
                args.sigma = float(sys.argv[i + 1])
                i += 1
            except (IndexError, ValueError):
                print("Error: Invalid sigma value.")
                sys.exit(1)
        elif option in ['-I', '--invert']:
            args.invert = True
        elif option in ['-P', '--plus']:
            args.plus = True
        elif i > 0 and not option.startswith('-') and option.endswith(
            ('.png', '.jpg', '.jpeg')):
            try:
                args.image = option
            except (IndexError, ValueError):
                print("Error: Image file not provided or file format invalid.")
                sys.exit(1)
        i += 1  # Move this outside the conditions to ensure the loop continues

    return args


#--------------------------------
#          print_help()
#--------------------------------
def print_help():
    # Commands
    print("Commands:")
    print(
        "  python main.py [Options] IMAGE   Convert image to unicode art, valid format: PNG, JPG, JPEG"
    )
    print("  python main.py [Tags]")
    # Introduction
    print("\nIntroduction:")
    print("  Convert an image to Unicode art.")
    # Tags
    print("\nTags:")
    print("  -v, --version      Show the version")
    print("  -h, --help         Show helping informations")
    print("  -w, --webui        Open webUI")
    print("  -s, --share        Share in public")
    # Options
    print("\nOptions:")
    #print("  -O,  --output [String]         Output directory, default: output/")
    print("  -S,  --scale [Float]           Scale factor, default: 1")
    print("  -CA, --contrast_alpha [Float]  Contrast alpha, default: 1.0")
    print("  -CB, --contrast_beta [Int]     Contrast beta, default: 0")
    print("  -SG, --sigma [Float]           Sharpen sigma, default: 1")
    print(
        "  -I,  --invert                  Invert the image to opposite color, default: False"
    )
    print(
        f"  -P,  --plus                    Use advanced algorthm for generating"
    )


#--------------------------------
#         print_version()
#--------------------------------
def print_version():
    # Print the application name using pyfiglet
    print(pyfiglet.figlet_format(__application__, font=LARRY3D))

    # Print version information
    print(f"version: {__version__}")

    # Read and print the content of the LICENSE file
    with open("LICENSE", "r", encoding="utf-8") as license_file:
        license_content = license_file.read()
        print(f"\n{license_content}")
