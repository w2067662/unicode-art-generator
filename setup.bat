@echo off
setlocal enabledelayedexpansion

REM Set the path to your Python executable
set PYTHON_EXE=python
REM Set the name of your virtual environment
set VENV_NAME=myenv

REM Check if virtualenv is installed
if not exist %VENV_NAME%\Scripts\activate.bat (
    echo Installing virtualenv...
    %PYTHON_EXE% -m pip install virtualenv
    if %errorlevel% neq 0 (
        echo Error installing virtualenv. Press any key to exit...
        pause
        exit /b 1
    )
)

REM Create and activate the virtual environment
if not exist %VENV_NAME% (
    echo Creating virtual environment...
    %PYTHON_EXE% -m virtualenv %VENV_NAME%
    if %errorlevel% neq 0 (
        echo Error creating virtual environment. Press any key to exit...
        pause
        exit /b 1
    )
)

echo Activating virtual environment...
call %VENV_NAME%\Scripts\activate
if %errorlevel% neq 0 (
    echo Error activating virtual environment. Press any key to exit...
    pause
    exit /b 1
)

REM Install dependencies
echo Installing dependencies...
pip install -r requirements.txt
if %errorlevel% neq 0 (
    echo Error installing dependencies. Press any key to exit...
    pause
    exit /b 1
)

REM Run your Python program
echo Running your Python program...
python main.py
if %errorlevel% neq 0 (
    echo Error running your Python program. Press any key to exit...
    pause
    exit /b 1
)

REM Deactivate the virtual environment
echo Deactivating virtual environment...
deactivate
if %errorlevel% neq 0 (
    echo Error deactivating virtual environment. Press any key to exit...
    pause
    exit /b 1
)

echo Environment setup and program execution complete.
pause
exit /b 0
