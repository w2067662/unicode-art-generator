# -*- coding: utf-8 -*-

#--------------------------------
#           Chars List
#--------------------------------
# List Default
default = ['.', '-', '*', '/', '+', '=', '&', '$', '@']

# List of Braille Patterns
braille_patterns = [
    "⠀", "⠁", "⠂", "⠃", "⠄", "⠅", "⠆", "⠇", "⠈", "⠉", "⠊", "⠋", "⠌", "⠍", "⠎", "⠏",
    "⠐", "⠑", "⠒", "⠓", "⠔", "⠕", "⠖", "⠗", "⠘", "⠙", "⠚", "⠛", "⠜", "⠝", "⠞", "⠟",
    "⠠", "⠡", "⠢", "⠣", "⠤", "⠥", "⠦", "⠧", "⠨", "⠩", "⠪", "⠫", "⠬", "⠭", "⠮", "⠯",
    "⠰", "⠱", "⠲", "⠳", "⠴", "⠵", "⠶", "⠷", "⠸", "⠹", "⠺", "⠻", "⠼", "⠽", "⠾", "⠿",
    "⡀", "⡁", "⡂", "⡃", "⡄", "⡅", "⡆", "⡇", "⡈", "⡉", "⡊", "⡋", "⡌", "⡍", "⡎", "⡏",
    "⡐", "⡑", "⡒", "⡓", "⡔", "⡕", "⡖", "⡗", "⡘", "⡙", "⡚", "⡛", "⡜", "⡝", "⡞", "⡟",
    "⡠", "⡡", "⡢", "⡣", "⡤", "⡥", "⡦", "⡧", "⡨", "⡩", "⡪", "⡫", "⡬", "⡭", "⡮", "⡯",
    "⡰", "⡱", "⡲", "⡳", "⡴", "⡵", "⡶", "⡷", "⡸", "⡹", "⡺", "⡻", "⡼", "⡽", "⡾", "⡿",
    "⢀", "⢁", "⢂", "⢃", "⢄", "⢅", "⢆", "⢇", "⢈", "⢉", "⢊", "⢋", "⢌", "⢍", "⢎", "⢏",
    "⢐", "⢑", "⢒", "⢓", "⢔", "⢕", "⢖", "⢗", "⢘", "⢙", "⢚", "⢛", "⢜", "⢝", "⢞", "⢟",
    "⢠", "⢡", "⢢", "⢣", "⢤", "⢥", "⢦", "⢧", "⢨", "⢩", "⢪", "⢫", "⢬", "⢭", "⢮", "⢯",
    "⢰", "⢱", "⢲", "⢳", "⢴", "⢵", "⢶", "⢷", "⢸", "⢹", "⢺", "⢻", "⢼", "⢽", "⢾", "⢿",
    "⣀", "⣁", "⣂", "⣃", "⣄", "⣅", "⣆", "⣇", "⣈", "⣉", "⣊", "⣋", "⣌", "⣍", "⣎", "⣏",
    "⣐", "⣑", "⣒", "⣓", "⣔", "⣕", "⣖", "⣗", "⣘", "⣙", "⣚", "⣛", "⣜", "⣝", "⣞", "⣟",
    "⣠", "⣡", "⣢", "⣣", "⣤", "⣥", "⣦", "⣧", "⣨", "⣩", "⣪", "⣫", "⣬", "⣭", "⣮", "⣯",
    "⣰", "⣱", "⣲", "⣳", "⣴", "⣵", "⣶", "⣷", "⣸", "⣹", "⣺", "⣻", "⣼", "⣽", "⣾", "⣿",
]

# List of digits
digits = [" ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

# List of symbols
symbols = [" ", "!", "~", "/", "`", ".", "|", "*", "(", ")", "-", "_", "+", "=", "[", "]", "{", "}", ";", ":", "'", "\"", "<", ">", ",", "%", "#", "?", "&", "$", "@"]

# List of Greek alphabet characters
greek_alphabets = [
    " ", "Α", "Β", "Γ", "Δ", "Ε", "Ζ", "Η", "Θ", "Ι", "Κ", "Λ", "Μ", "Ν", "Ξ", "Ο", "Π", "Ρ", "Σ", "Τ", "Υ", "Φ", "Χ", "Ψ", "Ω",
    "α", "β", "γ", "δ", "ε", "ζ", "η", "θ", "ι", "κ", "λ", "μ", "ν", "ξ", "ο", "π", "ρ", "σ", "τ", "υ", "φ", "χ", "ψ", "ω"
]

# List of English alphabet characters
english_alphabets = [
    " ", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
]

# List of English uppercase alphabet characters
english_alphabets_upper = [
    " ", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
]

# List of English lowercase alphabet characters
english_alphabets_lower = [
    " ", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
]

#--------------------------------
#         Chars List Map
#--------------------------------
# Define constant names
DEFAULT = "Default"
BRAILLE_PATTERNS = "Braille Patterns"
DIGITS = "Digits"
SYMBOLS = "Symbols"
GREEK_ALPHABETS = "Greek Alphabets"
ENGLISH_ALPHABETS = "English Alphabets"
ENGLISH_ALPHABETS_UPPERCASE = "English Alphabets Uppercase"
ENGLISH_ALPHABETS_LOWERCASE = "English Alphabets Lowercase"

# Define constant maps for character types
CHARS_LIST_MAP = {
    DEFAULT: default,
    BRAILLE_PATTERNS: braille_patterns,
    DIGITS: digits,
    SYMBOLS: symbols,
    GREEK_ALPHABETS: greek_alphabets,
    ENGLISH_ALPHABETS: english_alphabets,
    ENGLISH_ALPHABETS_UPPERCASE: english_alphabets_upper,
    ENGLISH_ALPHABETS_LOWERCASE: english_alphabets_lower,
}

#--------------------------------
#           Chars Map
#--------------------------------
AT = "At"
DOLLAR = "Dollar"
HASH = "Hash"
FULL_BLOCK = "Full Block"
QUESTION_MARK = "Question Mark"
XI = "Xi"
UPPERCASE_XI = "Uppercase Xi"
DELTA = "Delta"
THETA = "Theta"
OMEGA = "Omega"

# Define constant maps for characters
CHARS_MAP = {
    AT: '@',
    DOLLAR: '$',
    HASH: '#',
    FULL_BLOCK: '⣿',
    QUESTION_MARK: '?',
    XI: 'ξ',
    UPPERCASE_XI: 'Ξ',
    DELTA: 'Δ',
    THETA: 'Θ',
    OMEGA: 'ω',
}


#--------------------------------
#    get_selected_chars_list()
#--------------------------------
def get_selected_chars_list(selected_chars):
    """
    Get the list of characters based on the selected characters.

    Parameters:
    - selected_chars: List of selected characters from the Dropdown.

    Returns:
    - List of characters to be included.
    """
    # Return the Seleted chars list
    if selected_chars in CHARS_LIST_MAP:
        return CHARS_LIST_MAP[selected_chars]

    # If no certain type, return default list
    return CHARS_LIST_MAP[DEFAULT]

#--------------------------------
#         reverse_list()
#--------------------------------
def reverse_list(input_list):
    """
    Reverse the order of elements in a list.

    Parameters:
    - input_list: The input list to be reversed.

    Returns:
    - The reversed list.
    """
    return input_list[::-1]
