#!/bin/bash

# Set the name of your virtual environment
VENV_NAME=myenv

# Check if the virtual environment exists
if [ ! -d "$VENV_NAME" ]; then
    echo "Virtual environment not found. Setting up environment..."
    ./setup.sh
    if [ $? -ne 0 ]; then
        echo "Error setting up the environment. Press any key to exit..."
        read -n 1 -s
        exit 1
    fi
fi

# Activate the virtual environment
echo "Activating virtual environment..."
source $VENV_NAME/bin/activate
if [ $? -ne 0 ]; then
    echo "Error activating the virtual environment. Press any key to exit..."
    read -n 1 -s
    exit 1
fi

# Run your Python program with the provided arguments
python main.py -w -s "$@"
if [ $? -ne 0 ]; then
    echo "Error running the Python program. Press any key to exit..."
    read -n 1 -s
    exit 1
fi

# Deactivate the virtual environment
echo "Deactivating virtual environment..."
deactivate
if [ $? -ne 0 ]; then
    echo "Error deactivating the virtual environment. Press any key to exit..."
    read -n 1 -s
    exit 1
fi

echo "Script execution complete."
read -n 1 -s
exit 0
