@echo off

REM Set the name of your virtual environment
set VENV_NAME=myenv

REM Check if the virtual environment exists
if not exist %VENV_NAME% (
    echo Virtual environment not found. Setting up environment...
    call setup.bat
    if %errorlevel% neq 0 (
        echo Error setting up the environment. Press any key to exit...
        pause
        exit /b 1
    )
)

REM Activate the virtual environment
echo Activating virtual environment...
call %VENV_NAME%\Scripts\activate
if %errorlevel% neq 0 (
    echo Error activating the virtual environment. Press any key to exit...
    pause
    exit /b 1
)

REM Run your Python program with the provided arguments
python main.py -w -s %*
if %errorlevel% neq 0 (
    echo Error running the Python program. Press any key to exit...
    pause
    exit /b 1
)

REM Deactivate the virtual environment
echo Deactivating virtual environment...
deactivate
if %errorlevel% neq 0 (
    echo Error deactivating the virtual environment. Press any key to exit...
    pause
    exit /b 1
)

echo Script execution complete.
pause
exit /b 0
