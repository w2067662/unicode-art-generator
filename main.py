# -*- coding: utf-8 -*-

from args import get_args, print_help, print_version, __version__
from logic import adjust_image, img_to_unicode_logic, img_to_unicode_plus_logic, read_image, text_to_large_text_logic, gif_to_colored_unicode_gif_logic
from chars import CHARS_LIST_MAP, CHARS_MAP, get_selected_chars_list, reverse_list, BRAILLE_PATTERNS, DEFAULT, DOLLAR, QUESTION_MARK, THETA, UPPERCASE_XI
from figlet_styles import FIGLET_STYLES, SMISOME1, STOP
from const import *
import gradio as gr


#--------------------------------
#         img_to_unicode()
#--------------------------------
def img_to_unicode(image, scale, contrast_alpha, contrast_beta,
                   sharpen_strength, chars_to_include, invert):
    # If chars_to_include is empty, set selected_chars to CHARS_LIST_MAP[DEFAULT]
    selected_chars_list = get_selected_chars_list(chars_to_include)

    # Reverse the list
    selected_chars_list = reverse_list(selected_chars_list)

    # Call adjust_image function
    adjusted_image = adjust_image(image, contrast_alpha, contrast_beta,
                                  sharpen_strength, invert)

    # Call logic function
    output_text, output_file = img_to_unicode_logic(adjusted_image,
                                                    chars=selected_chars_list,
                                                    scale=scale,
                                                    image_path=image)

    return adjusted_image, output_text, output_file


#--------------------------------
#      img_to_unicode_plus()
#--------------------------------
def img_to_unicode_plus(image, scale, contrast_alpha, contrast_beta,
                        sharpen_strength, chars_to_include, invert):
    # If chars_to_include is empty, set selected_chars to CHARS_LIST_MAP[DEFAULT]
    selected_chars_list = get_selected_chars_list(chars_to_include)

    # Call adjust_image function
    adjusted_image = adjust_image(image, contrast_alpha, contrast_beta,
                                  sharpen_strength, invert)

    # Call logic function
    output_text, output_file = img_to_unicode_plus_logic(
        adjusted_image,
        chars=selected_chars_list,
        scale=scale,
        image_path=image)

    return adjusted_image, output_text, output_file


#--------------------------------
#   gif_to_colored_unicode_gif()
#--------------------------------
def gif_to_colored_unicode_gif(gif_path, duration, char, loop):
    output_gif_file = gif_to_colored_unicode_gif_logic(gif_path,
                                                       duration=duration,
                                                       char=CHARS_MAP[char],
                                                       loop=loop)

    return output_gif_file, output_gif_file


#--------------------------------
#      text_to_large_text()
#--------------------------------
def text_to_large_text(text, style):
    # Call text_to_large_text_logic()
    return text_to_large_text_logic(text, style)


#--------------------------------
#   Image to Unicode (Interface)
#--------------------------------
img2unicode = gr.Interface(
    fn=img_to_unicode,
    examples=[
        ["images/1.jpg", 2, 0.8, -7, 50, BRAILLE_PATTERNS, True],
        ["images/2.jpg", 1.4, 1.2, 40, 0.1, BRAILLE_PATTERNS, True],
        ["images/3.png", 2, 1.74, -55, 50, BRAILLE_PATTERNS, True],
        ["images/4.jpg", 2, 1.5, -15, 11.7, BRAILLE_PATTERNS, False],
    ],
    inputs=[
        gr.Image(label="Image to Convert"),
        gr.Slider(0.5, 2, value=1, label="Scale"),
        gr.Slider(0.1, 2, value=1, label="Contrast (alpha)"),
        gr.Slider(-100, 100, value=0, label="Contrast (beta)"),
        gr.Slider(0.1,
                  50,
                  value=1,
                  label="Sharpen (sigma)",
                  info="Make the edge more clear"),
        gr.Dropdown(list(CHARS_LIST_MAP.keys()),
                    value=DEFAULT,
                    label="Chars",
                    info="Chars to be included"),
        gr.Checkbox(value=False, label="Invert", info="Invert the image")
    ],
    outputs=[
        gr.Image(label="Adjusted Image"),
        gr.Code(label="Unicode Art"),
        gr.File(label="TXT File")
    ],
    allow_flagging="never",  # Remove flagging button
    api_name=IMG_TO_UNICODE,
)

#----------------------------------
#  Image to Unicode + (Interface)
#----------------------------------
img2unicode_plus = gr.Interface(
    fn=img_to_unicode_plus,
    examples=[
        ["images/1.jpg", 2, 0.8, -7, 50, BRAILLE_PATTERNS, True],
        ["images/2.jpg", 1.4, 1.2, 40, 0.1, BRAILLE_PATTERNS, True],
        ["images/3.png", 2, 1.74, -55, 50, BRAILLE_PATTERNS, True],
        ["images/4.jpg", 2, 1.5, -15, 11.7, BRAILLE_PATTERNS, False],
    ],
    inputs=[
        gr.Image(label="Image to Convert"),  # Read as ndarray
        gr.Slider(0.5, 2, value=1, label="Scale"),
        gr.Slider(0.1, 2, value=1, label="Contrast (alpha)"),
        gr.Slider(-100, 100, value=0, label="Contrast (beta)"),
        gr.Slider(0.1,
                  50,
                  value=1,
                  label="Sharpen (sigma)",
                  info="Make the edge more clear"),
        gr.Dropdown([BRAILLE_PATTERNS],
                    value=BRAILLE_PATTERNS,
                    label="Chars Type"),
        gr.Checkbox(value=False, label="Invert", info="Invert the image")
    ],
    outputs=[
        gr.Image(label="Adjusted Image"),
        gr.Code(label="Unicode Art"),
        gr.File(label="TXT File")
    ],
    allow_flagging="never",  # Remove flagging button
    api_name=IMG_TO_UNICODE_PLUS,
)

#-----------------------------------------
#  GIF to Colored Unicode GIF (Interface)
#-----------------------------------------
gif2colored_unicode_gif = gr.Interface(
    fn=gif_to_colored_unicode_gif,
    examples=[["gifs/1.gif", 0.1, DOLLAR, True],
              ["gifs/2.gif", 0.2, UPPERCASE_XI, False],
              ["gifs/3.gif", 0.1, QUESTION_MARK, True],
              ["gifs/4.gif", 0.1, THETA, True]],
    inputs=[
        gr.File(type="filepath",
                label="GIF to Convert"),  # Read as filepath str
        gr.Slider(0.1,
                  0.5,
                  value=0.1,
                  label="Duration",
                  info="Time between frames"),
        gr.Dropdown(list(CHARS_MAP.keys()), value=DOLLAR, label="Char"),
        gr.Checkbox(value=True, label="Loop"),
    ],
    outputs=[
        gr.Image(label="Preview Image"),
        gr.File(label="Colored Unicode GIF File"),
    ],
    allow_flagging="never",  # Remove flagging button
    api_name=GIF_TO_COLORED_UNICODE_GIF,
)

#--------------------------------
#  Text to Large Text (Interface)
#--------------------------------
text2large_text = gr.Interface(
    fn=text_to_large_text,
    examples=[["Unicode Art Generator", STOP], ["Hi", SMISOME1]],
    inputs=[
        gr.Textbox(label="Text to Convert"),
        gr.Dropdown(FIGLET_STYLES, value=SMISOME1, label="Style")
    ],
    outputs=[gr.Code(label="Large Text"),
             gr.File(label="TXT File")],
    allow_flagging="never",  # Remove flagging button
    api_name=TEXT_TO_LARGE_TEXT,
)

#--------------------------------
#        Tabbed (Interface)
#--------------------------------
web_ui = gr.TabbedInterface(
    [img2unicode, img2unicode_plus, gif2colored_unicode_gif, text2large_text],
    [
        "Image to Unicode", "Image to Unocode +", "GIF to Colored Unicode GIF",
        "Text to Large Text"
    ],
    #theme='Medguy/base2' # Use Theme (Optional)
)


#--------------------------------
#         Launch Web UI
#--------------------------------
def start_web_ui(share):
    web_ui.queue(max_size=10)
    web_ui.launch(show_api=True, share=share)


#--------------------------------
#              Main
#--------------------------------
if __name__ == '__main__':
    args = get_args()

    if args.help:
        print_help()
    elif args.version:
        print_version()
    elif args.webui:
        start_web_ui(args.share)
    elif not args.image:
        print_help()
    else:
        if args.plus:
            img_to_unicode_plus(image=read_image(args.image),
                                scale=args.scale,
                                contrast_alpha=args.contrast_alpha,
                                contrast_beta=args.contrast_beta,
                                sharpen_strength=args.sigma,
                                chars_to_include=BRAILLE_PATTERNS,
                                invert=args.invert)
        else:
            img_to_unicode(image=read_image(args.image),
                           scale=args.scale,
                           contrast_alpha=args.contrast_alpha,
                           contrast_beta=args.contrast_beta,
                           sharpen_strength=args.sigma,
                           chars_to_include=BRAILLE_PATTERNS,
                           invert=args.invert)
