# -*- coding: utf-8 -*-

from PIL import Image, ImageDraw, ImageFont, ImageSequence
from const import *
import time, os, cv2, pyfiglet, imageio, numpy as np

##########################################
#                                        #
# OpenCV and Pillow Preprocess functions #
#                                        #
##########################################


#--------------------------------
#         read_gif()
#--------------------------------
def read_gif(path):
    """
    Read a GIF from the specified file path using the Python Imaging Library (PIL).

    Parameters:
    - path: File path of the GIF.

    Returns:
    - Loaded image (PIL Image object).
    """
    # PIL object
    return Image.open(path)


#--------------------------------
#         read_image()
#--------------------------------
def read_image(path):
    """
    Read an image from the specified file path using OpenCV.

    Parameters:
    - path: File path of the image.

    Returns:
    - Loaded image (numpy array).
    """
    # cv2.imread([filepath], [-1|0|1]) , -1: cv2.IMREAD_UNCHANGED(containing alpha), 0: cv2.IMREAD_GRAYSCALE, 1: cv2.IMREAD_COLOR
    return cv2.imread(path, cv2.IMREAD_UNCHANGED)


#--------------------------------
#            gray()
#--------------------------------
def gray(image):
    """
    Convert the image to grayscale.

    Parameters:
    - image: Input image (numpy array).

    Returns:
    - Grayscale image (numpy array).
    """
    # Convert the image to grayscale
    return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


#--------------------------------
#          contrast()
#--------------------------------
def contrast(image, alpha=0.0, beta=0.0):
    """
    Adjust the contrast of the image.

    Parameters:
    - image: Input image (numpy array).
    - alpha: Contrast control (scaling factor). Default is 0.0.
    - beta: Brightness control (shifting factor). Default is 0.0.

    Returns:
    - Contrast-adjusted image (numpy array).
    """
    # Adjust the contrast of the image using the formula: new_pixel = alpha * original_pixel + beta
    return cv2.convertScaleAbs(image, alpha=alpha, beta=beta)


#--------------------------------
#           sharpen()
#--------------------------------
def sharpen(image, sigma=50):
    """
    Apply unsharp masking to sharpen the input image.

    Parameters:
    - image: Input image (numpy array).
    - sigma: Standard deviation for Gaussian blur. Default is 50.

    Returns:
    - Sharpened image (numpy array).
    """
    # Apply Gaussian blur to the input image
    blur_img = cv2.GaussianBlur(image, (0, 0), sigma)

    # Apply unsharp masking to create the sharpened image
    unsharp_masking = cv2.addWeighted(image, 1.5, blur_img, -0.5, 0)

    return unsharp_masking


#--------------------------------
#            invert()
#--------------------------------
def invert(image_array):
    """
    Invert the colors of the input image array.

    Parameters:
    - image_array: Input image array (numpy array).

    Returns:
    - Inverted image array (numpy array).
    """
    # Invert the colors by subtracting each pixel value from the maximum intensity value
    inverted_image = 255 - image_array

    return inverted_image


###################################
#                                 #
#       Supporting Functions      #
#                                 #
###################################


#--------------------------------
#          gray_to_char()
#--------------------------------
def gray_to_char(gray, chars):
    """
    Map grayscale value to corresponding character.

    Parameters:
    - gray: Grayscale value (0 to 255).
    - chars: List of characters to map to grayscale values.

    Returns:
    - Mapped character based on the grayscale value.
    """
    # Map 256 grayscale (gray) to characters (ascii_char)
    length = len(chars)
    unit = 256.0 / length
    index = int(gray / unit)

    # Use min to ensure the index is within bounds
    return chars[min(index, length - 1)]


#--------------------------------
#          gray_to_bits()
#--------------------------------
def gray_to_bits(gray_image):
    """
    Convert a gray scale image to a binary matrix (1 and 0) based on pixel intensity.

    Parameters:
    - gray_image: Grayscale image (numpy array).

    Returns:
    - Binary matrix (numpy array) representing the input image.
    """
    # Define a threshold value to classify pixels as 1 or 0
    threshold = 128

    # Apply thresholding to create a binary matrix
    binary_matrix = (gray_image < threshold).astype(int)

    return binary_matrix


#--------------------------------
#         resize_image()
#--------------------------------
def resize_image(image_array):
    """
    Resize the input image array to ensure its dimensions are multiples of 4 in rows and multiples of 2 in columns. (4M x 2N matrix)

    Parameters:
    - image_array: Input image array (numpy array).

    Returns:
    - Resized image array (numpy array).
    """
    # Calculate the new dimensions
    new_rows = (image_array.shape[0] // 4) * 4
    new_cols = (image_array.shape[1] // 2) * 2

    # Resize the image_matrix
    resized_image = image_array[:new_rows, :new_cols]

    return resized_image


#--------------------------------
#       save_output_txt_file()
#--------------------------------
def save_output_txt_file(txt, folder_name):
    """
    Save the generated text to a file with a timestamped filename in the specified folder.

    Parameters:
    - txt: Generated text content.
    - folder_name: Name of the folder to store the output file.

    Returns:
    - File path of the saved output.
    """
    # Check if the output directory exists, create it if not
    if not os.path.exists(DEFAULT_OUTPUT_DIRECTORY):
        os.makedirs(DEFAULT_OUTPUT_DIRECTORY)

    # Check if the folder path exists, create it if not
    folder = os.path.join(DEFAULT_OUTPUT_DIRECTORY, folder_name)
    if not os.path.exists(folder):
        os.makedirs(folder)

    # Generate timestamp for unique file name
    timestamp = time.strftime("%Y%m%d%H%M%S")

    # Output the character art to a file with timestamped file name
    output_file = os.path.join(folder, f"{folder_name}_{timestamp}.txt")

    # Write file
    with open(output_file, 'w', encoding='utf-8') as f:
        f.write(txt)

    print(f"Output file saved at: {output_file}")

    return output_file


#--------------------------------
#      save_output_gif_file()
#--------------------------------
def save_output_gif_file(gif_frames, folder_name, duration=0.1, loop=True):
    """
    Save the generated GIF frames to a file with a timestamped filename in the specified folder.

    Parameters:
    - gif_frames: List of frames to be saved as a GIF.
    - folder_name: Name of the folder to store the output GIF file.
    - duration: Duration of each frame in seconds.
    - loop: Boolean indicating whether the GIF should loop.

    Returns:
    - File path of the saved output GIF.
    """
    # Check if the output directory exists, create it if not
    if not os.path.exists(DEFAULT_OUTPUT_DIRECTORY):
        os.makedirs(DEFAULT_OUTPUT_DIRECTORY)

    # Check if the folder path exists, create it if not
    folder = os.path.join(DEFAULT_OUTPUT_DIRECTORY, folder_name)
    if not os.path.exists(folder):
        os.makedirs(folder)

    # Generate timestamp for a unique file name
    timestamp = time.strftime("%Y%m%d%H%M%S")

    # Use os.path.join to construct the correct file path
    output_gif_file = os.path.join(folder, f"{folder_name}_{timestamp}.gif")

    # Save the frames as a GIF file
    if loop:
        imageio.mimsave(output_gif_file, gif_frames, duration=duration, loop=0)
    else:
        imageio.mimsave(output_gif_file, gif_frames, duration=duration)

    print("Processing completed. Output gif saved at:", output_gif_file)

    return output_gif_file


#--------------------------------
#         adjust_image()
#--------------------------------
def adjust_image(image, contrast_alpha, contrast_beta, sharpen_strength,
                 invert_image):
    """
    Adjust the input image by applying contrast adjustment, sharpening, and converting to grayscale.

    Parameters:
    - image: Input image (numpy array).
    - contrast_alpha: Alpha parameter for contrast adjustment.
    - contrast_beta: Beta parameter for contrast adjustment.
    - sharpen_strength: Strength of sharpening.
    - invert_image: Boolean indicating whether to invert the colors.

    Returns:
    - Adjusted image (numpy array).
    """
    # Apply contrast adjustment
    image = contrast(image, alpha=contrast_alpha, beta=contrast_beta)

    # Apply sharpening
    image = sharpen(image, sharpen_strength)

    # Convert the image to grayscale
    image = gray(image)

    # Invert Image
    if invert_image:
        image = invert(image)

    return image


#--------------------------------
#       binary_to_braille()
#--------------------------------
def binary_to_braille(binary_matrix):
    """
    Convert a binary_matrix to Unicode Braille characters.

    *Notice*: If the value within binary_matrix is not 0 and 1, might cause wrong calculation
    when encoding Braille Patterns.

    Parameters:
    - binary_matrix: Binary_matrix (numpy array) with pixel values 0 and 1.
    
    Returns:
    - Unicode string representing the Braille characters.
    """
    # Resize the image_matrix if needed
    resized_image = resize_image(binary_matrix)

    braille_chars = []

    # Define the Braille dot positions
    dot_positions_hex = [[1, 8], [2, 16], [4, 32], [64, 128]]

    # Iterate through the resized image in 4x2 blocks
    for row in range(0, resized_image.shape[0], 4):
        for col in range(0, resized_image.shape[1], 2):
            # Extract the 4x2 block
            block = resized_image[row:row + 4, col:col + 2]

            # Calculate the decimal value from the reversed bits
            decimal_value = sum([
                dot_positions_hex[i][0] * block[i, 0] +
                dot_positions_hex[i][1] * block[i, 1]
                for i in range(len(dot_positions_hex))
            ])

            # Convert the decimal value to hexadecimal
            hex_value = hex(decimal_value)[2:].upper()

            # Assuming hex_value is a string representing the hexadecimal value, e.g., '8A'
            # Convert the hexadecimal value to Unicode Braille character
            unicode_char = chr(int(hex_value, 16) + 0x2800)

            # Append the Unicode character to the result list
            braille_chars.append(unicode_char)

    # Reshape the list into a matrix-like structure
    result_matrix = np.array(braille_chars).reshape(
        (resized_image.shape[0] // 4, resized_image.shape[1] // 2))

    # Convert the result matrix to a string
    result_str = '\n'.join([''.join(row) for row in result_matrix])

    return result_str


###################################
#                                 #
#        Interface Logics         #
#                                 #
###################################


#--------------------------------
#      img_to_unicode_logic()
#--------------------------------
def img_to_unicode_logic(image,
                         chars,
                         scale=1.0,
                         output_directory=DEFAULT_OUTPUT_DIRECTORY,
                         image_path=None):
    # Resize the image using the scale parameter
    img = cv2.resize(image, (0, 0),
                     fx=scale * 0.1,
                     fy=scale * 0.05,
                     interpolation=cv2.INTER_NEAREST)

    # Get the Image Width, Height
    height, width = img.shape  # Get the dimensions of the img array

    # Convert Image to Char (unicode)
    txt = ""
    for i in range(height):
        for j in range(width):
            # Check if indices are within bounds
            if i < img.shape[0] and j < img.shape[1]:
                txt += gray_to_char(img[i, j], chars)
            else:
                print(f"Index out of bounds: ({i}, {j})")

        txt += '\n'
    print(txt)

    return txt, save_output_txt_file(txt, IMG_TO_UNICODE)


#--------------------------------
#   img_to_unicode_plus_logic()
#--------------------------------
def img_to_unicode_plus_logic(image,
                              chars,
                              scale=1.0,
                              output_directory=DEFAULT_OUTPUT_DIRECTORY,
                              image_path=None):
    # Resize the image using the scale parameter
    img = cv2.resize(image, (0, 0),
                     fx=scale * 0.2,
                     fy=scale * 0.2,
                     interpolation=cv2.INTER_NEAREST)

    # Convert gray scale image to bits matrix
    image_array = gray_to_bits(img)

    # Encode the matrix to braille patterns matrix
    txt = binary_to_braille(image_array)
    print(txt)

    return txt, save_output_txt_file(txt, IMG_TO_UNICODE_PLUS)


#-------------------------------------
#  gif_to_colored_unicode_gif_logic()
#-------------------------------------
def gif_to_colored_unicode_gif_logic(gif_filepath, duration, char, loop):

    def process_frame(frame):
        # Resize each frame to scale 0.1
        resized_frame = frame.resize(
            (int(frame.width * 0.1), int(frame.height * 0.1)))

        color_matrix = [[0 for _ in range(resized_frame.width)]
                        for _ in range(resized_frame.height)]

        for y in range(resized_frame.height):
            for x in range(resized_frame.width):
                pixel_color = resized_frame.getpixel((x, y))
                color_matrix[y][x] = pixel_color

        return color_matrix

    def generate_colored_image(color_matrix):
        # Determine image size based on the dimensions of the matrix
        image_width = len(color_matrix[0])
        image_height = len(color_matrix)

        # Create a new image with a white background
        image = Image.new("RGB", (image_width * 15, image_height * 15),
                          color="black")
        draw = ImageDraw.Draw(image)

        # Set the font and size
        font_size = 15  # Adjust the font size as needed
        font_style = "font_style/DejaVuSans/DejaVuSans.ttf"
        font = ImageFont.truetype(font_style, font_size)

        for y in range(image_height):
            for x in range(image_width):
                color = color_matrix[y][x]

                # Ensure color is a tuple
                if not isinstance(color, tuple):
                    color = (color, )

                text = char  # Character to draw
                text_color = tuple(color)  # Convert color list to tuple

                # Calculate the position to draw the text
                text_position = (x * font_size, y * font_size)

                # Draw the text on the canvas with the specified color
                draw.text(text_position, text, font=font, fill=text_color)

        return image

    def convert_gif_to_frames(gif_image):
        frames = []
        try:
            while True:
                frames.append(gif_image.copy())
                gif_image.seek(gif_image.tell() + 1)
        except EOFError:
            pass
        return frames[1:]  # Skip the first frame

    gif_image = read_gif(gif_filepath)
    gif_frames = convert_gif_to_frames(gif_image)

    processed_frames = []
    for frame in gif_frames:
        color_matrix = process_frame(frame)
        colored_image = generate_colored_image(color_matrix)
        processed_frames.append(colored_image)

    return save_output_gif_file(processed_frames,
                                GIF_TO_COLORED_UNICODE_GIF,
                                duration=duration,
                                loop=loop)


#--------------------------------
#    text_to_large_text_logic()
#--------------------------------
def text_to_large_text_logic(text,
                             style,
                             output_directory=DEFAULT_OUTPUT_DIRECTORY,
                             image_path=None):
    # Use pyfiglet to convert text to large Unicode text art
    large_text = pyfiglet.figlet_format(text, font=style)
    print(large_text)

    return large_text, save_output_txt_file(large_text, TEXT_TO_LARGE_TEXT)
