# 🤪 Unicode Art Generator

## 🤓 Introduction

This tool makes it easy to **turn your images into cool Unicode art**. It's perfect for anyone who wants to add a special touch to their text or make eye-catching designs. Whether you're expressing your creativity or improving visual communication, this tool is a great way to be your resolution.

## 🤔 Motivation

My motivation stems from the desire to generate Unicode art using meme images or gifs. Despite searching for online tools, I couldn't find one that met my expectations. Therefore, I made the decision to develop my own solution.

## 🥳 Web UI

The web UI of the Unicode Art Generator is based on the **[Gradio](https://www.gradio.app/)** framework. I chose **Gradio** over other frameworks because it **offers a more comprehensive set of interfaces, components, and file types**, facilitating the easier construction of the web UI.

![image to unicode](screenshots/preview1.gif)

## 😬 How to Use

**Prerequisites:**

- Python (Version **3.10.6** is used in this project)

**Usage:**

1. Clone this repository.
2. Run **`webUI.bat`**. The script will execute **`setup.bat`** to automatically set up the required dependencies for this program.
3. Open http://127.0.0.1:7860 in your browser.

**Commands:**

- Its also possible to generate the unicode art from image by commands:

```bash
python main.py -S 2 -CA 1.5 -CB 50 -SG 50 -I -P images/2.jpg  # Sample command
```

```bash
Commands:
  python main.py [Options] IMAGE   Convert image to unicode art, valid format: PNG, JPG, JPEG
  python main.py [Tags]

Introduction:
  Convert an image to Unicode art.

Tags:
  -v, --version      Show the version
  -h, --help         Show helping informations
  -w, --webui        Open webUI
  -s, --share        Share in public

Options:
  -S,  --scale [Float]           Scale factor, default: 1
  -CA, --contrast_alpha [Float]  Contrast alpha, default: 1.0
  -CB, --contrast_beta [Int]     Contrast beta, default: 0
  -SG, --sigma [Float]           Sharpen sigma, default: 1
  -I,  --invert                  Invert the image to opposite color, default: False
  -P,  --plus                    Use advanced algorthm for generating
```

## 🤯 Algorithm

### 🔄 Normal

The normal algorithm involves converting grayscale values into characters and rearranging the matrix accordingly. The order of characters in the list may impact the final result.
![Normal algorithm](documents/normal_algo1.png)

This algorithm can also be applied to **Braille Patterns** using the same rules:
![Normal algorithm on Braille Patterns](documents/normal_algo2.png)

**Advantages:**

- Effectively to convert the **shade** of the image.

**Disadvantages:**

- May struggle to produce clear Unicode art if the preprocessed image **lacks clarity**.
- Characters must possess **distinctiveness in terms of area**. Using visually similar characters with close apparent areas reduces effectiveness.

### ⏫ Advanced

**[Braille Patterns](https://en.wikipedia.org/wiki/Braille_Patterns)** are Unicode characters encoded by the algorithm. The coding follows the standard dot-numbering 1 to 8, with each dot individually capable of being raised or not, resulting in 256 different patterns. The Unicode name of a specific pattern mentions the raised dots, and the mapping is computed by adding together the hexadecimal values of the dots raised.

The dots within **Braille Patterns** follow a specific sequence, each with its corresponding value for encoding.

![Sequence and Value of Braille Pattern](documents/gray_to_braillepattern_algo2.png)

The algorithm first resizes the image to a 4M x 2N matrix, converts it to grayscale, then transforms it into binary bits before encoding it into **Braille Patterns** Unicode.

![Gray to Braille Pattern](documents/gray_to_braillepattern_algo1.png)

For example, calculate the product sum of the 4 x 2 matrix and determine the corresponding **Braille Patterns** based on its hexadecimal value:

![Encode Brailled Pattern](documents/gray_to_braillepattern_algo3.png)

The same rule applies to other characters:

![Other Characters](documents/gray_to_braillepattern_algo4.png)

**Advantages:**

- Effectively captures the **edge** of the image.

**Disadvantages:**

- The representation of image **shade** may be influenced by the defined character matrix. When exclusively using **Braille Patterns**, the depiction of image **shade** may not yield optimal results.

## 😏 Comparison

From the following results, it's evident that the **advanced** algorithm better captures image details by emphasizing edges compared to the **normal** algorithm.

**Sample Input**

![input image](images/1.jpg)

**Image to Unicode** (normal)

```
⣥⣥⣤⣤⣤⣥⣥⣣⣣⣢⣤⣤⢸⢬⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣼⣿⢰⣾⣾⣾⣾⣾⣾⣾⣽⣽⣽⣽⣽⣽⣽⣼⣼⣼⣼⣼⣼⣼⣼⣼⣼⣼
⣥⣥⣤⣤⣤⣥⣥⣥⣤⣤⣤⣤⣣⣜⣯⣐⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣼⣹⣸⣿⣿⣴⣹⣿⣿⣿⣿⣾⣾⣾⣾⣾⣾⣾⣾⣾⣽⣽⣽⣽⣽⣽⣽⣽⣼⣼⣼⣼⣼⣼⣼⣼⣼
⣥⣥⣥⣥⣥⣦⣦⣤⣦⣢⣦⣢⣢⣢⣦⢶⠀⣿⣿⣿⣿⣿⣿⠚⣴⣿⣿⣿⣿⣿⣿⣿⣿⣲⣥⣿⠀⣿⣼⠀⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣽⣽⣽⣽⣽⣽⣽⣽⣽⣽
⣦⣣⣣⣣⣣⣤⣤⣒⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠞⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣸⣿⣿⣭⣾⣽⣶⢠⣾⡜⣾⢺⣻⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾⣾
⣤⣤⣢⣢⣤⣤⣤⣜⠀⠀⠀⠀⠀⠀⠀⣿⣿⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾⣾⣾⣾⣾⢼⣺⢶⡔⣾⣾⣾⣾⣾⣾⣾⣾⣿⣾⣿⣿⣿⣿⣿⣿⣿
⣤⣤⣤⣤⣤⣤⣤⣬⠀⠀⠀⠀⠀⣿⣿⣿⠀⠀⣿⣿⣿⣿⣿⣿⣦⣻⣿⣿⣿⣿⣿⣿⣺⣰⣿⣿⣿⣿⣿⣾⣾⣾⣾⣾⣼⣺⣿⣿⡴⣐⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣣⣣⣣⣣⣣⣣⣢⣤⢜⠀⠀⣃⢮⣿⠀⠀⣿⣿⣿⣿⣿⣿⣿⢕⣿⣿⣿⣿⣿⣿⣿⣿⣸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣻⣿⣿⣿⣷⣞⣸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣢⣢⣤⣢⣠⣢⣢⣢⣢⠀⣿⣿⢷⠀⠨⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣼⠀⣽⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣡⣡⣠⣠⣜⣠⣟⣞⣣⡌⣔⠀⠀⣛⣿⢐⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢲⣞⣼⣾⡴⣽⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾⠀⣯⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿
⣠⣠⣜⣜⣢⣡⣠⣢⣥⣨⠀⢘⡜⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠮⣼⠱⣿⣼⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣂⣿⣿⣿⣿⡴⠀⣧⠀⠀⠀⠀⠀⣒⣿⣿⣿
⣚⣘⣝⣝⣜⣜⣜⣜⣜⣩⣿⣿⣫⣿⣿⣿⣿⣿⣿⢆⣿⣿⣿⣿⣿⣿⣿⡔⣼⣿⢺⣿⢀⣺⣿⣿⣿⣿⣿⣿⣳⣿⣿⣿⣿⣿⣿⣶⣿⣿⣿⣿⣿⠀⣿⣬⠀⠀⠀⠀⣿⣿⣿⣿
⢬⣜⣝⣝⣤⣝⣜⣚⣜⣿⣿⣼⢴⣿⡲⣿⣿⣿⢗⣾⠡⣶⣿⣿⠍⣿⣡⡃⣿⣿⢀⣿⠀⣻⣿⣿⣿⣿⣿⣿⣞⣨⣿⣿⣿⣿⣿⣿⡆⣿⣿⣿⣴⣿⠀⣷⣿⠀⠀⣹⣿⣿⣿⣿
⣖⣆⣊⣫⣔⣔⣌⣮⢚⣿⣋⣿⣚⣿⣿⣿⣿⣿⠔⣖⣿⣶⣏⣢⣒⣺⣂⣿⣿⣿⣦⣾⠀⣸⣿⣿⣿⣿⣿⣿⡐⣲⣭⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⣙⠀⠥⣿⣿⣿⣿⣿⣿
⣞⢺⣤⣃⣎⣙⣚⠆⣿⣿⣿⣿⣿⣿⣿⣿⣿⠖⣕⣿⣿⣿⣾⠶⣿⣿⣿⣿⣿⣿⣾⢸⠔⡮⣾⣿⣿⣿⣿⣶⠜⣞⠺⣿⣿⣿⣿⣿⣿⣼⣿⣿⣿⣾⣿⠀⣿⣼⣿⣿⣿⣿⣿⣿
⣄⣊⢠⢰⣢⡲⢒⣿⣿⣝⣿⣿⣺⣿⣿⣿⣿⢸⢒⢃⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣺⣿⣋⢮⣡⣿⣿⢶⣿⣿⠶⣸⠀⢘⣿⣿⣿⣸⣿⣿⣿⣹⣾⣿⠀⣿⣿⣿⣿⣿⣿⣿⣿
⣀⣜⡰⣡⣊⢎⣝⣾⣿⣀⣿⣿⣼⣿⣴⣿⢣⠀⠀⠀⠀⠀⣟⣢⠶⣿⣿⣿⣿⣿⣿⣿⣿⣆⣻⣎⣸⢕⣲⣿⢮⣿⡭⣿⡤⣾⣿⣿⣿⣸⣿⣿⢾⣰⣿⡒⣿⣿⣿⣿⣿⣿⣿⣿
⣘⢖⣌⢶⡺⠀⢢⣿⣿⣿⣿⣿⣿⣭⣿⣿⠌⣵⣿⣿⣿⣽⣽⡙⣿⠈⣳⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾⣸⣭⠶⣿⣱⠫⣶⣿⣿⣿⣿⣫⣿⣾⣿⣬⣼⣾⣿⣿⣿⣿⣿⣿⣿⣿
⣠⡲⣥⡫⢚⣿⣃⣿⣿⣿⣿⣿⣿⣼⣺⣿⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣼⣿⣿⢜⣿⣿⣿⣿⢤⡊⡤⣿⣿⣿⣿⣿⣿⡢⡜⣿⣿⣿⣸⣿⣋⣿⣿⣿⣺⣿⣿⣿⣿⣿⣿⣿⣿
⣞⣁⢦⢧⣿⣿⢖⣿⡦⣿⣿⣿⣿⣿⣾⣿⠀⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⢌⣿⣫⢬⣿⣿⣿⣿⣰⣾⠀⠀⠀⠣⣿⡚⣹⣿⡄⣿⠪⢾⣿⣿⣵⣿⣿⣿⣷⢤⣿⣿⣿⣿⣿⣿⣿
⣀⣬⡺⡂⣿⣿⢬⣿⣤⣿⣿⣿⣿⣿⣿⢬⠀⣿⣿⣿⣿⣿⣿⡥⣿⣿⣿⣿⣿⣿⣹⣿⣿⣿⣲⣿⣿⣿⣿⣿⡐⣿⠀⠀⢐⣒⣘⣚⢂⣿⣮⢺⣿⣿⣿⣿⠀⣾⣿⣿⣿⣿⣿⣿
⡟⣎⢌⣿⢘⣿⣿⣿⣾⣿⣿⣿⣿⣿⣿⣿⣜⣺⣿⣿⣿⣿⡺⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⣳⣕⠾⢐⢌⣼⣿⣿⣿⣿⢍⣿⣿⣿⣿⣿⣿⣿
⢫⢂⠶⣿⣿⣸⣿⣾⣪⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣼⣿⣿⣸⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣖⣔⣠⠀⣐⢩⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣧⡴⣖⣿⣿⢞⣿⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⢲⣿⣿⣿⣿⢝⡰⡺⣿⢢⣿⡨⣲⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣢⣖⣷⢐⠀⣔⣾⣿⣿⣿⣿⣿⣿⣏⣮⣾⣿⣿⣿⣿⣿
⣢⢀⣝⣠⣿⣿⣰⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠏⣿⣿⣿⣿⣘⠀⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣎⣌⠌⣍⣿⣿⣿⣿⣿⣿⣿⣿⣿⣚⠒⣿⣿⣿⣿⣿⣿
⣙⠴⣞⣢⣠⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣲⣿⣿⣿⣿⣿⣰⠀⠀⠭⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣚⣘⢼⣍⢾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣆⢼⣶⣿⣿⣿⣿⣿
⢞⣪⣢⣤⣢⣤⣤⣿⡨⣿⣿⣿⢖⣿⣿⣿⣿⣿⣿⡜⣿⣿⣿⣱⠌⣿⣿⢬⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣪⣖⢤⣜⣖⣀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣚⣚⣓⣿⣿⣿⣿⣿⣿
⡦⣜⣔⣨⡒⣢⣤⣥⣠⣦⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⣨⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣘⣜⠀⢸⣦⢣⣝⢾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣮⣒⢲⣇⠥⣿⣿⣿⣿⣿
⢦⢢⣸⣪⠇⠀⣜⣦⣡⣥⣿⣿⣿⣿⣿⣿⡚⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠊⡄⣦⡫⣞⣈⣊⣢⣢⣠⡰⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣼⣖⣘⠀⣉⢼⣿⣿⣿⣿⣿
⣰⢌⣮⢖⣶⡾⣺⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾⣿⢅⠀⠀⠀⣮⣾⠀⣞⡤⢼⣡⣬⠵⣪⣠⢴⣟⡴⣿⣿⣿⣿⣿⣿⣿⣿⣾⣺⣠⣤⣟⣑⣑⣐⣐⣿⣿⣿⣿
⣷⢴⣞⣪⣐⠀⠀⣿⠀⠀⠀⠀⠀⠀⠀⠸⣿⣿⣿⣿⣏⡄⣨⣱⣿⠿⣳⣿⣿⣿⣿⣿⣿⣿⠼⣞⢂⣞⣡⡌⣿⣀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣖⣒⣃⣒⣨⠀⣗⣑⣔⠀⣿⣿⣿
```

**Image to Unicode+** (advanced)

```
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡵⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣵⡙⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣧⡝⣿⣿⣿⣿⣿⣿⣿⣿⡿⠿⣛⣛⣛⣉⣉⣉⣛⢛⡛⠿⢣⣿⣧⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⡿⠿⠿⢿⣿⣿⣿⣿⣿⢌⢿⣿⣿⠿⢛⣭⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣽⣸⣿⣿⣄⣛⣛⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⣶⡶⠂⢀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾⣯⣾⣵⣩⡛⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣧⠀⠀⠀⠀⠀⢀⢸⣿⠛⠀⣠⣿⣿⣿⣿⣿⣿⡿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⣝⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⣸⣿⡿⠁⠀⣼⣿⣿⣿⣿⣿⡿⣯⣿⣿⣿⣿⣿⣿⣿⣣⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⡆⠀⣴⣿⣿⠋⠀⣤⣿⣿⣿⣿⣿⣿⣿⣾⣿⣿⣿⣿⣿⣿⣿⢣⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡝⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⢤⣿⠟⠁⢀⣾⣯⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠋⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡌⢛⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⡌⠋⠀⡴⣿⣿⣾⣿⣿⣿⣿⣳⣿⣿⣿⣿⣿⣿⣿⡟⣵⢧⣿⣿⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⢿⡆⠈⠉⠛⠿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⣠⣾⣼⣿⣿⣿⣿⣿⣿⢯⣿⣿⣿⣿⣿⣿⣿⡟⣼⣿⢸⣿⣇⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⣿⣿⣿⣿⡆⠘⣷⣄⠀⠀⠀⠀⣩⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⢡⣿⢧⣿⣿⢿⣿⣿⣿⡟⣾⠟⣿⣿⣿⣿⣿⡿⣼⣿⣿⣿⣿⢸⣿⣿⣿⣿⣿⣿⣿⡟⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⢻⣯⠀⠀⠀⣴⣿⣿⣿⣿
⣿⣿⣿⣿⣷⣿⣿⣿⢣⣿⣿⣿⣿⣿⣾⣿⣿⣿⡸⣫⣾⢻⣿⣿⠀⣿⢱⣤⢹⣿⢸⣿⢸⣿⣿⣿⣿⣿⣿⣿⢻⢹⣿⣿⣿⣿⣿⣿⣸⣿⣿⣿⣿⣧⠀⣿⠇⠀⣼⣿⣿⣿⣿⣿
⣿⣿⣿⡇⣿⣿⣿⡟⣾⣿⣿⣿⣿⣿⣿⣿⣿⠇⣾⣿⡿⣃⡻⡇⣿⠛⣿⣿⣧⣿⣏⣿⠀⣿⣿⣿⣿⣿⣿⣿⣸⣧⣿⣿⣿⣿⣿⣿⡇⣿⣿⣿⣿⣿⡀⢻⢆⣾⣿⣿⣿⣿⣿⣿
⡟⣿⡇⣷⣿⡇⡻⣸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣸⣿⣿⣿⣿⣿⣾⣿⣿⣿⣿⣿⣿⣿⡹⡄⣜⢿⣿⣿⣿⣿⣿⣖⢻⣞⣿⣿⣿⣿⣿⣏⣿⣿⣿⣿⣿⣇⢨⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⢰⣿⡟⢀⢠⣿⣿⣿⣿⣿⣯⣿⣿⣿⡏⠓⠶⠾⢭⣿⣛⢿⣿⣿⣿⣿⣿⣿⣿⣷⡣⣿⣷⣟⢿⣿⡿⣿⣿⣔⢻⣜⣼⣿⣿⣿⣿⣿⣿⣿⣿⣧⣿⢸⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣇⣸⡿⢣⣞⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢁⣶⣤⣤⡤⠄⣉⠟⢮⣟⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣬⣛⠻⢹⣿⡄⣿⡆⣿⣿⣿⣿⣿⣿⣿⣿⢻⣿⡜⣿⣿⣿⣿⣿⣿⣿⣿
⣿⢸⣿⢳⠸⣾⣽⣿⡟⣿⣿⣿⣿⣿⣿⣿⢸⣿⣿⣿⣿⣿⣿⣶⣵⣈⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣼⣿⣿⡜⣧⡿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⣿⣿⣿⣿⣿⣿⣿⣿
⣏⢸⡇⢸⢰⣿⣿⣿⢿⣿⣿⣿⣿⣿⣿⣿⢸⣿⣿⣿⣿⣿⣿⣭⣭⣭⣿⣿⡭⢽⣚⣿⣿⣿⠿⠷⣶⠶⠯⣽⣛⡿⣿⣿⣴⡆⣿⣿⠿⣿⣿⣽⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⢻⢧⣿⣿⢸⣿⣸⣿⣿⣿⣿⣿⣿⣿⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣞⣿⣿⣿⣿⣿⡻⣿⣯⣵⣖⡠⢄⡈⠙⠪⡿⣿⣬⣽⣶⠃⣿⢿⣿⣿⣿⣿⣿⢹⣿⣿⣿⣿⣿⣿⣿
⢻⡇⡘⣾⣿⣿⡿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡈⣿⣿⣿⣿⣿⣟⣾⣿⣿⣿⣿⣿⣷⣿⣿⣿⣿⣝⢿⣿⣿⣿⣿⣾⣵⡪⣂⠺⣻⣿⡟⢲⣟⣿⣿⣿⣿⣿⣿⢸⣿⣿⣿⣿⣿⣿⣿
⢸⠇⢠⣿⣾⣿⣧⣿⢿⣿⣿⣿⣿⣿⣿⣿⡇⣿⣿⣿⣿⡿⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣬⣿⠑⣼⡾⣾⣿⣿⣿⣿⣿⡜⣿⣿⣿⣿⣿⣿⣿
⣿⠘⣼⣿⣷⣿⣿⣻⣟⣿⣿⣿⣿⣿⣿⣿⣷⢻⣿⣿⣿⡇⡿⢛⣟⣿⢿⣛⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠁⡀⣝⣿⣿⣿⣿⣿⣿⣿⡇⣿⣿⣿⣿⣿⣿⣿
⡟⠀⣿⣿⣿⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⣿⣿⣿⣧⣶⠈⠘⠛⠿⢿⣾⠝⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⢠⣾⣿⣿⣿⣿⣿⣿⣿⣿⡇⢻⣿⣿⣿⣿⣿⣿
⡇⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡜⣿⣿⣿⣿⣧⠀⠀⠀⠀⢀⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢟⡏⠰⢫⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⢸⣿⣿⣿⣿⣿⣿
⣷⣸⣿⣿⣿⣿⣿⣿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢻⣿⣿⣿⡹⣷⣤⣤⡾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣫⣭⣭⢣⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⡇⣿⣿⣿⣿⣿⣿
⢸⡇⣿⣿⣿⣿⣿⣿⣜⣿⣿⣿⣾⣿⣿⣿⣿⣿⣿⡎⠻⣿⣿⣿⣮⣿⣵⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠛⠱⠿⠟⢫⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⣿⢻⣿⣿⣿⣿⣿
⢸⣿⢹⣿⠈⢿⣿⣿⣷⣻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⢄⠀⠉⠛⠻⠿⢿⣿⣿⣿⣿⣿⣿⣿⠿⠿⢛⣛⢡⢄⣿⡇⠉⡇⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢱⣿⡜⣿⣿⣿⣿⣿
⣿⢹⡇⢻⠀⢠⡛⢿⠟⠃⠻⣿⣿⣿⣿⣿⣾⣽⣛⡿⠳⠽⠦⠄⠀⠀⠀⠀⠀⠀⢀⢠⡀⣼⡟⣼⣿⠯⢥⣿⣿⡇⠠⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣫⢸⣿⣷⢻⣿⣿⣿⣿
⣿⢸⡇⣶⡅⠞⠃⠀⠀⠐⠀⠈⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿⠿⣯⡻⣶⣤⣴⣶⣿⡏⢸⣿⣜⣿⡿⠋⣢⣿⠿⠙⡇⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢟⣵⡟⣿⣿⣿⣧⢿⣿⣿⣿
⡏⣻⢇⠟⠁⢀⠀⠁⠀⠀⠀⠀⠀⠀⠀⠰⣯⣭⣭⣭⣷⣾⣿⣿⣿⣾⣟⢿⣿⣿⢧⣿⣿⣿⣎⠱⣾⠿⣫⢰⣥⣛⢸⣿⣿⣿⣿⣿⣿⣿⣿⢟⣽⣿⣿⢃⣬⡻⣿⣿⣎⢿⣿⣿
```

# 😵 Extra

The Unicode Art Generator comes with an additional feature called **GIF to Colored Text GIF**, allowing users to convert GIFs into color text-filtered GIFs.

![Gif to Colored Text Gif](gifs/sample_output.gif)

# 🤗 Reference

Algorithm solution and information in this project are referenced from the links provided in [reference](reference.txt).
